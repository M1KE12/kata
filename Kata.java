public class Kata {
  
  public static String solution(String str) {
    String word;
    word = str;
    StringBuilder invertedWord = new StringBuilder(word);
    invertedWord.reverse();
    
   return invertedWord.toString();
  }

}