public class Kata2 {

  public String winner(String[] deckSteve, String[] deckJosh) {
   String[] playerOne = deckSteve;
   String[] playerTwo = deckJosh;
   int[] PlayerOnevalue = new int[playerOne.length];
   int[] PlayerTwovalue = new int[playerOne.length];
   int playerOnePoint = 0;
   int playerTwoPoint = 0;

   for(int i = 0; i < playerOne.length;i++){//convierte las letras en valores numericos
    switch (playerOne[i]) {
      case "T":
        playerOne[i] = "10";
        break;

      case "J":
        playerOne[i] = "11";
        break;

      case "Q":
        playerOne[i] = "12";
        break;

      case "K":
        playerOne[i] = "13";
        break;
      
      case "A":
        playerOne[i] = "21";
        break;
    
    
      default:
      playerOne[i] = playerOne[i];
        break;
    }
     switch (playerTwo[i]) {
      case "T":
        playerTwo[i] = "10";
        break;

      case "J":
        playerTwo[i] = "11";
        break;

      case "Q":
        playerTwo[i] = "12";
        break;

      case "K":
        playerTwo[i] = "13";
        break;
      
      case "A":
        playerTwo[i] = "21";
        break;
    
    
      default:
      playerTwo[i] = playerTwo[i];
        break;
    }
   }

   /*------------convertir cadenas a enteros------------ */

   for(int j =0; j < playerOne.length;j++){
    PlayerOnevalue[j] = Integer.parseInt(playerOne[j]);
    PlayerTwovalue[j] = Integer.parseInt(playerTwo[j]);
   }

   /*-----------------winner---------------------- */

 for(int y = 0; y < playerOne.length;y++){
    if (PlayerOnevalue[y] > PlayerTwovalue[y]) {
      playerOnePoint += 1;
    }else if(PlayerTwovalue[y] > PlayerOnevalue[y]){
      playerTwoPoint +=1;
    }
  }
  if (playerOnePoint > playerTwoPoint) {
    return "Steve wins "+playerOnePoint+" to "+playerTwoPoint;
  }else if(playerTwoPoint > playerOnePoint){
    return "Josh wins "+playerTwoPoint+" to "+playerOnePoint ;
  }
    return "Tie";
  }
  
}